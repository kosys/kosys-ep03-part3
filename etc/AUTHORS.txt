################################################################################
# This file is contributors list of this directory and its sub directories. 
# See <repository root>/AUHTORS.txt for all contributors.
#-------------------------------------------------------------------------------
# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。
# すべての貢献者は、<リポジトリルート>/AUTHORS.txtをご覧ください。 
################################################################################
#
# Note: 
#   - Revision history is available at 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part1  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part2  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part3  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part4  
#
# 注記: 
#   - 更新履歴は以下からご覧いただけます。 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part1  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part2  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part3  
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep03-part4  
#


### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ここまで自動生成です。上記を編集しないでください ###
井二かける	コンテ
熊谷 悠平（株式会社ラック）	シナリオ技術監修
今井 志有人（株式会社ラック）	シナリオ技術監修
西部 修明（株式会社ラック）	シナリオ技術監修
山坂 匡弘（株式会社ラック）	シナリオ技術監修
仲上 竜太（株式会社ラック）	シナリオ技術監修
谷口 隼祐（株式会社ラック）	シナリオ技術監修
クニキチ	シナリオ校正協力
玉虫型偵察器	シナリオ校正協力
yokonaha103	シナリオ校正協力
川内康雄（ｉ法律事務所）	法律関係支援
頭座技富	3DCG
クニキチ	3DCG
RootGentle	3DCG
くまのぐり	3DCG
Sota＠./make	3DCG
sacorama	3DCG
PiT	3DCG
小澤 佑太	3DCG
るみあ	キャラクターデザイン
夏野未来	キャラクターデザイン
なめたけ	キャラクターデザイン
リンゲリエ	キャラクターデザイン
乃樹坂くしお	キャラクターデザイン
如月ほのか	キャラクターデザイン
安坂　悠	キャラクターデザイン
武本譲治	キャラクターデザイン
０たか	キャラクターデザイン
okok3	キャラクターデザイン
麦の人	制服設定
ふぶき	鉄道設定
京姫鉄道合同会社	クラウドファンディング窓口,原作マンガ制作